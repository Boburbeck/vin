# VIN Micro Service
[![build status](https://Boburbeck@bitbucket.org/Boburbeck/vin.git)](https://Boburbeck@bitbucket.org/Boburbeck/vin.git)

Server side Vin application.

### Tech


I used:

* [Django] - is a high-level Python Web framework
* [Django REST framework] - Django REST framework is a powerful and flexible toolkit for building Web APIs
* [Postgresql] - open source object-relational database system

And many other libraries.

### Installation

Dillinger requires [Python](https://www.python.org) v3.6+.

Install the dependencies and devDependencies and start the server.
```sh
$ git clone https://Boburbeck@bitbucket.org/Boburbeck/vin.git
$ cd vin
$ pip install virtualenv
$ virtualenv .venv
$ source .venv/bin/activate (if linux)
$ .venv\Scripts\activate (if windows)
$ pip install -r requirements.txt
$ python manage.py migrate
```
### Postgres
```
$ su postgres
$ psql
postgres=# CREATE DATEBASE vin;
postgres=# CREATE USER vin_user WITH ENCRYPTED PASSWORD 'vin_password';
postgres=# ALTER USER vin_user WITH SUPERUSER';
postgres=# GRANT ALL PRIVILEGES ON DATABASE vin to vin_user;
```
Note. After you have pulled the project, you have to make app directory as SOURCE ROOT
```sh
Assunming that you are using PyCharm
Right click on apps folder
Go to "Mark Directory as" below in the list of options
Select "Source root"
apps folder should turn to blue
```
### Development
```sh
$ python manage.py runserver
```

### Documentation
Api http://localhost:8000/docs/



[Django]: <https://www.djangoproject.com/>
[Django REST framework]: <http://www.django-rest-framework.org/>
[Postgresql]: <https://www.postgresql.org/>
