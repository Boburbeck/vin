import os
import json
import pycodestyle

from django.test import TestCase
from django.conf import settings

from development.utils import get_files_for_checking


class PyCodeStyleTest(TestCase):
    def test_check_files(self):
        """Test that we conform to PEP-8."""
        app_dir = os.path.join(settings.BASE_DIR, 'apps')
        source = get_files_for_checking(app_dir)

        for item in source:
            style = pycodestyle.StyleGuide(quite=True, ignore=['E501'])
            result = style.check_files([item])
            message = '%s:%s:%s %s' % (
                item,
                result.counters['physical lines'],
                result.counters['logical lines'],
                json.dumps(result.messages, sort_keys=True, indent=4)
            )
            self.assertEqual(0, result.total_errors, message)
