import requests
from vin.settings import DECODE_URL


def get_vin_codes(vin):
    url = '%s%s/' % (DECODE_URL, vin)
    r = requests.get(url=url)
    data = r.json()
    if data.get('error'):
        return dict(vin='No Vehicle Associated with this VIN Number'), False
    vin_obj = get_or_create_vin(data)
    return vin_obj, True


def get_or_create_vin(data):
    from main.models import Vin
    vin = data.get('vin')
    year = data['years'][0].get('year', 'No year')
    make = data['make'].get('name', 'No Make')
    model = data['model'].get('name', 'No Name')
    type = data['engine'].get('type', 'No Type')
    manufacturer = data.get('manufacturer', 'No Manufacturer')
    horse_power = data['engine'].get('horsepower', 0)
    vin_obj, created = Vin.objects.get_or_create(
        vin=vin,
        defaults={
            'year': year,
            'make': make,
            'model': model,
            'type': type,
            'manufacturer': manufacturer,
            'horse_power': horse_power,
        }
    )
    return vin_obj
