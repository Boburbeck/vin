# Generated by Django 3.0 on 2019-12-12 20:39

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='vin',
            options={'ordering': ('-id',), 'verbose_name': 'Vin', 'verbose_name_plural': 'Vin Numbers'},
        ),
        migrations.RemoveField(
            model_name='vin',
            name='color',
        ),
        migrations.RemoveField(
            model_name='vin',
            name='dimensions',
        ),
        migrations.RemoveField(
            model_name='vin',
            name='weight',
        ),
        migrations.AddField(
            model_name='vin',
            name='horse_power',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='vin',
            name='manufacturer',
            field=models.CharField(max_length=255, null=True),
        ),
        migrations.AlterField(
            model_name='vin',
            name='make',
            field=models.CharField(max_length=255, null=True),
        ),
        migrations.AlterField(
            model_name='vin',
            name='model',
            field=models.CharField(max_length=255, null=True),
        ),
        migrations.AlterField(
            model_name='vin',
            name='type',
            field=models.CharField(max_length=255, null=True),
        ),
        migrations.AlterField(
            model_name='vin',
            name='vin',
            field=models.CharField(max_length=255, unique=True),
        ),
        migrations.AlterField(
            model_name='vin',
            name='year',
            field=models.CharField(max_length=10, null=True),
        ),
    ]
