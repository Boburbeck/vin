from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase


class VinTest(APITestCase):
    fixtures = [
        'vin.yaml',
    ]
    list_url = 'main:vehicle-list'
    detail_url = 'main:vehicle-detail'
    data = dict(
        vin='JT2AC52L8W0306798',
        year='1998',
        make='Toyota',
        model='1998 Toyota Tercel',
        type='1.5L L4 DOHC 16V',
        manufacturer='Toyota',
        horse_power=150
    )

    def setUp(self):
        pass

    def test_list(self):
        url = reverse(self.list_url)
        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(2, len(response.data['results']))

    def test_vin_number(self):
        """Note, if the postman mock server is set (based on the the files sent), this test below works as expected"""
        """Otherwise, it will fail"""

        base_url = reverse(self.list_url)
        url = '%snumber/?vin=JHMCM563X7C022272' % base_url
        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['make'], 'Honda')

        url = '%snumber/?vin=YV1LW564XW2348768' % base_url
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['vin'], 'No Vehicle Associated with this VIN Number')

    def test_search(self):
        base_url = reverse(self.list_url)
        url = '%s?search=1J4FN68S5WL125730' % base_url
        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(1, len(response.data['results']))

        url = '%s?search=JT2AC52L8W0306798' % base_url
        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(0, len(response.data['results']))

    def test_filter_by_date(self):
        base_url = reverse(self.list_url)
        url = '%s?begin=2019-05-05' % base_url
        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(1, len(response.data['results']))

        url = '%s?end=2019-05-05' % base_url
        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(2, len(response.data['results']))

    def test_filter_by_year(self):
        base_url = reverse(self.list_url)
        url = '%s?year=1998' % base_url
        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(1, len(response.data['results']))

        url = '%s?year=2013' % base_url
        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(0, len(response.data['results']))

    def test_filter_by_make(self):
        base_url = reverse(self.list_url)
        url = '%s?make=Lamborghini' % base_url
        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(1, len(response.data['results']))

        url = '%s?make=JEEP' % base_url
        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(1, len(response.data['results']))

        url = '%s?make=TOYOTA' % base_url
        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(0, len(response.data['results']))

    def test_detail(self):
        url = reverse(self.detail_url, kwargs={"pk": 1})
        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['vin'], 'ZHWUC1ZD2EL007291')

        return response.data

    def test_create(self):
        url = reverse(self.list_url)
        response = self.client.post(url, data=self.data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data['vin'], "JT2AC52L8W0306798")

    def test_create_fail(self):
        data = dict(
            year='1998',
            make='Toyota',
            model='1998 Toyota Tercel',
            type='1.5L L4 DOHC 16V',
            manufacturer='Toyota',
            horse_power=150
        )
        url = reverse(self.list_url)
        response = self.client.post(url, data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data['vin'][0], "This field is required.")

    def test_delete(self):
        url = reverse(self.detail_url, kwargs={"pk": 1})
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
