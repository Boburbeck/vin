from django.db import models
from main.models import BaseMeta
from main.models import BaseModel
from main.models import DeleteMixin


class Vin(BaseModel, DeleteMixin):
    vin = models.CharField(max_length=255, unique=True)
    year = models.CharField(max_length=10, null=True)
    make = models.CharField(max_length=255, null=True)
    model = models.CharField(max_length=255, null=True)
    type = models.CharField(max_length=255, null=True)
    manufacturer = models.CharField(max_length=255, null=True)
    horse_power = models.IntegerField(default=0)

    class Meta(BaseMeta):
        verbose_name = 'Vin'
        verbose_name_plural = 'Vin Numbers'

    def __str__(self):
        return "%s-%s" % (self.model, self.year)
