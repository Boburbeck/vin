# Django
from django.urls import path, include
from rest_framework.routers import DefaultRouter

from main.views import VinModelViewSet


router = DefaultRouter()
router.register(r'vehicle', VinModelViewSet, 'vehicle')

group_router = DefaultRouter()

urlpatterns = [
    path('', include(router.urls)),
]
