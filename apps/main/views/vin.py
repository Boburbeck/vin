# Django
from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.response import Response

# Project
from main.serializers import VinModelSerializer
from main.serializers import ValidateVinSerilizers
from main.models import Vin
from main.utils import get_vin_codes
from main.filters import VinFilterSet


class VinModelViewSet(viewsets.ModelViewSet):
    """
       ### Expected body
       list:
       Return a list of all Vin.
       Method GET
            {
                "id": 1,
                "vin": "ZHWUC1ZD2EL007291",
                "year: "2014",
                "make": "Lamborghini",
                "model": "2014 Lamborghini Aventador",
                "type": "COUPE 2-DR",
                "manufacturer": "Lamborghini",
                "horse_power": 205,
                "created_date": "2019-01-01 00:00:00+00:00",
                "modified_date": "2019-01-01 00:00:00+00:00",
            }
   ###Note
    + refer - **Send VIN number as shown below, it returns the vehicle details if the vehicle exists**
    + vin - **/api/v1/main/vehicle/number/?vin=ZHWUC1ZD2EL007291**

   ###Filter
    + Note - **Other (optional) Filters in the project**
    + begin - **Date** - **Returns all the data above starting from the begin date**
    + e.g - **2019-01-01** **Returns all the data data >= 2019-01-01**
    + E.G - **/api/v1/main/vehicle/?begin=2019-01-01**
    + end - **Date** - **Returns all the data below starting from the end date**
    + e.g - **2019-01-01** **Returns all the data data <= 2019-01-01**
    + E.G - **/api/v1/main/vehicle/?end=2019-01-01**
    + make - **/api/v1/main/vehicle/?make=Toyota**
    + manufacturer - **/api/v1/main/vehicle/?manufacturer=Toyota**

   ###Search
    + vin - **/api/v1/main/vehicle/?search=ZHWUC1ZD2EL007291**


   create:
   Create a new Net Vin instance.
   Method POST
   ### Request body
            {

                "vin": "ZHWUC1ZD2EL007291",
                "year: "2014",
                "make": "Lamborghini",
                "model": "2014 Lamborghini Aventador",
                "type": "COUPE 2-DR",
                "manufacturer": "Lamborghini",
                "horse_power": 205,
            }
   ###Fields
   + vin - **CharField** - **Required**

    delete:

    """
    queryset = Vin.objects.all()
    serializer_class = VinModelSerializer
    filterset_class = VinFilterSet
    search_fields = ('vin',)
    model = Vin

    @action(['GET'], detail=False)
    def number(self, request):
        serializer = ValidateVinSerilizers(data=request.GET)
        serializer.is_valid(raise_exception=True)
        vin = serializer.validated_data.get('vin')
        result, success = get_vin_codes(vin)
        if not success:
            return Response(result)
        serializer = VinModelSerializer(result, many=False)
        return Response(serializer.data)
