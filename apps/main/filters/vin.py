# Project
from main.filters import BaseFilter
from main.models import Vin


class VinFilterSet(BaseFilter):
    class Meta:
        model = Vin
        fields = ['year', 'make', 'manufacturer']
