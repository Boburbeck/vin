from rest_framework import serializers

# Project
from main.models import Vin


class VinSelectSerializer(serializers.ModelSerializer):
    class Meta:
        model = Vin
        fields = ('id', 'vin', 'model', 'year')


class VinModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = Vin
        fields = "__all__"


class ValidateVinSerilizers(serializers.Serializer):
    vin = serializers.CharField(max_length=200)
