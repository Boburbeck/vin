from django.contrib import admin
from main.models import Vin


class VinAdmin(admin.ModelAdmin):
    list_display = ('vin', 'year', 'make', 'type', 'manufacturer')
    list_display_links = ('vin',)
    list_filter = ('year', 'make', 'type', 'manufacturer')
    search_fields = ['year', 'make', 'type', 'manufacturer']
    list_editable = ('year', 'make', 'type', 'manufacturer')


admin.site.register(Vin, VinAdmin)
